# KNUMxGolemMLHackaton2022

## intro
Efekty naszej pracy na hackatonie KNUMxGolemMLHackaton2022 w zagadnieniu "few-shot learning".



W src w notebook znajdują się notebooki z kodem do klasyfikacji.

Weronika_pipe - test dla zestawu 2

Raff_pipe - test dla zestawu 1

Json załączone zarówno w repo jak i w formularzu.

Założeniem było przyjęcie, że robot i tak potrzebuje najpierw zdjąć to co nie jest przesłonięte, więc dokonujemy segmentacji jedynie rzeczy, które nie są przesłonięte.

Więcej w prezentacji o naszej metodologii


Na zbiorze walidacyjnym z naszej metodologii uzyskaliśmy Celność na poziomie 77,7%


## future 

- Walidacja na cały zbiorze danych
- wyczyszczenie kodu po chaosie pracy
- dalszy rozwój zapropnowanych przez nas rozwiązań dla funkcji dystansu i klasyfikacji
- Dalsze zagłębianie się w FSL.
